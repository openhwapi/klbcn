@echo off

rem Status Report.Txt为状态报告
rem *.EXTREP：是一个额外补充的文件，没用的，不用管，删了
rem *.RUL：包含了你的PCB涉及约束规则项目，与GERBER生产PCB没关系，删了
rem *.APR_LIB：是产生的嵌入式光圈（RX274X）文件，还有APT，是未设置为嵌入式光圈，也可以删了
del  "Status Report.Txt" *.EXTREP *.RUL *.APR_LIB *.REP

rem GM1、GM13、GM15是AD中元件的3D建模相关，无用删除        
del *.GM1 *.GM13 *.GM15                            

rem 底层走线层GBL，底层丝印层GBO，底层阻焊层GBS，板框层GKO，顶层走线层GTL，顶层丝印层GTO，顶层组焊层GTS                
md GERBER                                
move *.GBL GERBER
move *.GBO GERBER
move *.GBS GERBER 
move *.GKO GERBER 
move *.GTL GERBER
move *.GTO GERBER
move *.GTS GERBER

rem 提取钻孔层
rem GD1 是钻孔图，指明用何种钻头,指明钻何种孔，焊盘及过孔的钻孔孔径尺寸描述层。
rem GG1 DRILL GUIDE（钻孔定位层）：焊盘及过孔的钻孔的中心定位坐标层。
rem APR 是钻孔数据
rem 上面几个图层在生产电路板时无用，而需要NC DRILL导出的.DRR和.TXT文件
rem DRR规定钻的类型，TXT规定每个孔的位置及需要用的钻
md 钻孔
del *.GD1 *.GG1 *.apr
move *.DRR 钻孔
move *.TXT 钻孔
del *.LDP

rem 提取钢网文件，底层锡膏层（我的所有元件均在底层，因此删除了顶层锡膏层）
md 钢网                                    
move *.GTP 钢网
del  *.GBP

rem 删除顶层主焊盘.GPT和底层主焊盘.GPB 
del  *.GPT *.GPB