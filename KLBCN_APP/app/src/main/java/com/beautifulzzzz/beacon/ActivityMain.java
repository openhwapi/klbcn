package com.beautifulzzzz.beacon;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import java.util.Vector;

@SuppressLint("NewApi")
public class ActivityMain extends Activity implements SurfaceHolder.Callback {
    private SurfaceView mSurface;
    private SurfaceHolder mHolder;
    private BluetoothAdapter mBtAdapter;
    private boolean mScanning;// 标记是否正在搜索

    private Button btn;

    private Vector<BluetoothDevice> mDevicesVector;
    private Vector<Integer> mRSSIVector;
    private Vector<Paint>  mPaint;

    private int CanvasWidth,CanvasHeight;
    
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int REQUEST_ENABLE_BT = 1;
    private final static String TAG = ActivityMain.class.getSimpleName();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDevicesVector=new Vector<BluetoothDevice>();//向量
        mRSSIVector=new Vector<Integer>();
        paintInit();

        mSurface=(SurfaceView)findViewById(R.id.surface);
        mHolder = mSurface.getHolder();
        mHolder.addCallback(this);

        btInit();

        btn = (Button) findViewById(R.id.btn_search);
        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                scanLeDevice(true);
            }
        });
    }

    private void paintInit(){
        mPaint=new Vector<Paint>();
        Paint paint0 = new Paint();
        paint0.setAntiAlias(true);
        paint0.setStyle(Style.STROKE);
        paint0.setColor(Color.RED);
        mPaint.add(paint0);
        Paint paint1 = new Paint();
        paint1.setAntiAlias(true);
        paint1.setStyle(Style.STROKE);
        paint1.setColor(Color.GREEN);
        mPaint.add(paint1);
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setStyle(Paint.Style.STROKE);
        paint2.setColor(Color.BLUE);
        mPaint.add(paint2);
        Paint paint3 = new Paint();
        paint3.setAntiAlias(true);
        paint3.setStyle(Style.STROKE);
        paint3.setColor(Color.YELLOW);
        mPaint.add(paint3);
        Paint paint4 = new Paint();
        paint4.setAntiAlias(true);
        paint4.setStyle(Style.STROKE);
        paint4.setColor(Color.WHITE);
        mPaint.add(paint4);
        Paint paint5 = new Paint();
        paint5.setAntiAlias(true);
        paint5.setStyle(Style.STROKE);
        paint5.setColor(Color.LTGRAY);
        mPaint.add(paint5);
        Paint paint6 = new Paint();
        paint6.setAntiAlias(true);
        paint6.setStyle(Style.STROKE);
        paint6.setColor(Color.CYAN);
        mPaint.add(paint6);
    }

    /*
     * bluetooth init
     */
    private void btInit(){
        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        //为了确保设备上蓝牙能使用, 如果当前蓝牙设备没启用,弹出对话框向用户要求授予权限来启用
        if (!mBtAdapter.isEnabled()) {
            if (!mBtAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        //bluetooth permission for 6.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // TODO request success
                }
                break;
        }
    }

    /*
     * 用来搜索蓝牙设备的函数SCAN_PERIOD=10s周期
     * 信息接收在mLeScanCallback回调函数中
     */
    private void scanLeDevice(final boolean enable) {
        if(enable == mScanning)return;

        if (enable) {
            mScanning = true;
            if(false == thread.isAlive())thread.start();
            mBtAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            if(true == thread.isAlive())thread.stop();
            mBtAdapter.stopLeScan(mLeScanCallback);
        }
    }

    /*
     * Device scan callback.
     */
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device,final int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int index = mDevicesVector.indexOf(device);
                    if(-1 != index){
                        mRSSIVector.set(index,rssi);
                    }else{
                        mDevicesVector.add(device);
                        mRSSIVector.add(rssi);
                    }
                    Log.i(TAG, device.getName() + " " + device.getAddress() + " " + rssi);
                }
            });
        }
    };

    /*
     * A thread for draw the circle, whose size stands for rssi
     */
    Thread thread=new Thread(new Runnable() {
        @Override
        public void run() {
            while(true){
                draw();

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        private void draw() {
            Canvas canvas = mHolder.lockCanvas();
            CanvasWidth = canvas.getWidth();
            CanvasHeight = canvas.getHeight();
            canvas.drawRGB(0, 0, 0);
            String showText =null;

            for(int i=mRSSIVector.size()-1;i>=0;i--)
            {
                mPaint.get(i).setTextSize(CanvasHeight / 50);
                showText = String.format("%02d: %s --> %04d",i,mDevicesVector.get(i),mRSSIVector.get(i));
                if(mRSSIVector.get(i) == -1){//not find in this time
                    canvas.drawText(showText, 5, (i+1)*(CanvasHeight/50), mPaint.get(i));
                }else{
                    canvas.drawText(showText, 5, (i+1)*(CanvasHeight/50), mPaint.get(i));
                    canvas.drawCircle(CanvasWidth / 2, CanvasHeight / 2, CanvasWidth / 3 + mRSSIVector.get(i), mPaint.get(i)); //画圆圈
                    mRSSIVector.set(i,-1);
                }
            }
            mHolder.unlockCanvasAndPost(canvas);// 更新屏幕显示内容
        }
    });

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
